function get_test_sql(version){
    if (version=="v1"){
        return `select 1 as test`
    }
    if (version=="v2"){
        return `select 2 as test`
    }
}

module.exports = {
    get_test_sql
}