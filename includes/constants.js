const TABLE_NAME = "dataform.dim_Employee_view";

const DATASET = "twc-bidev.bi_star"

const DATAFORM = "dataform"

// const PROJECT_ID = `${dataform.projectConfig.vars.project}`

const VERSION = `${dataform.projectConfig.vars.version}`

// const DEFAULT_DATABASE = `${dataform.projectConfig.vars.defautlDatabase}`

module.exports = {
  TABLE_NAME,
  DATASET,
  DATAFORM,
  VERSION
};
