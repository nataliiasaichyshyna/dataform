operate("delete_calendar").queries("drop table if exists dataform.dim_Calendar").tags("delete");
operate("delete_customer").queries("drop table if exists dataform.dim_Customer").tags("delete");
operate("delete_employee").queries("drop table if exists dataform.dim_Employee").tags("delete");
operate("delete_exchange").queries("drop table if exists dataform.dim_ExchangeRateToBase").tags("delete");
operate("delete_item")    .queries("drop table if exists dataform.dim_Item").tags("delete");
operate("delete_location").queries("drop table if exists dataform.dim_Location").tags("delete");
operate("delete_vendor")  .queries("drop table if exists dataform.dim_Vendor").tags("delete");
operate("delete_sale")    .queries("drop table if exists dataform.flat_Sale_view").tags("delete");
operate("delete_test")    .queries("drop table if exists dataform.test_table").tags("delete");